# deta-ci-cd-example



## Getting started

Deta is a free cloud crafted with the developer and user experience at heart.

[Deta Micros](https://docs.deta.sh/docs/home/#deta-micros) (servers) are a lightweight but scalable cloud runtime tied to an HTTP endpoint. Currently Node.js and Python Micros are supported.

## Creating Your First Micro
You are able to follow [official docs](https://docs.deta.sh/docs/micros/getting_started#creating-your-first-micro) while creating your micro.

## Pipeline
The pipeline makes easy to deploy on each commits.

Privacy is everything. So, don't push your private information to repository. The [CI/CD Variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) is avaiable for providing environment variable.

### Required Variables for Pipeline
- DETA_TOKEN

Required for authorization. You can create a new token on Deta.

https://web.deta.sh/home/:your_username/

[Official Docs](https://docs.deta.sh/docs/cli/auth/#deta-access-tokens)


- DETA_PROG_INFO

Required for project information. You must a **File** variable. Its content should be same in `.deta/prog_info`.


## License
Read [LICENSE](./LICENSE) file.

